using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpCoin : PickUp
{
    private const string pickUpAnimation = "Shrinking";
    [SerializeField] private Animator _smallCoinAnimator;
    [SerializeField] private AudioSource _audioSource;

    protected override void PickedUp(Collider player)
    {
        GameEvents.Instance.PickedUpCoin();
        _audioSource.Play();
        _smallCoinAnimator.Play(pickUpAnimation);
        gameObject.SetActive(false);
    }
}
