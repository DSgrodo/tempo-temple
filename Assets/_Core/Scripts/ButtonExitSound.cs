using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonExitSound : MonoBehaviour
{
    public void PlaySound()
    {
        UIAudioPlayer.Instance.PlayButtonExit();
    }
}
