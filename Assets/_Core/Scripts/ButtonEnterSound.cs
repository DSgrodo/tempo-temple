using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEnterSound : MonoBehaviour
{
    public void PlaySound()
    {
        UIAudioPlayer.Instance.PlayButtonEnter();
    }
}
