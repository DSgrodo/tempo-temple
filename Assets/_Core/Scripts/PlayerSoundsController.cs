using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundsController : MonoBehaviour
{
    private const float minStepSoundPitch = 0.8f;
    private const float maxStepSoundPitch = 1.2f;

    [SerializeField] private AudioSource _stepSound;
    [SerializeField] private AudioSource _fallDeathSound;
    [SerializeField] private AudioSource _exhaleSound;
    [SerializeField] private AudioSource _commonDeathSound;

    private void Start()
    {
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void SubscribeToEvents()
    {
        GameEvents.Instance.OnPlayerFall += OnPlayerFallHandler;
        GameEvents.Instance.OnPlayerStepped += OnPlayerSteppedHandler;
        // GameEvents.Instance.OnShadowTrapActivated += OnShadowTrapActivatedHandler;
        // GameEvents.Instance.OnSpikesTrapActivated += OnSpikesTrapActivatedHandler;
        GameEvents.Instance.OnPlayerDestroyed += OnPlayerDestroyedHandler;
    }

    private void UnsubscribeFromEvents()
    {
        GameEvents.Instance.OnPlayerFall -= OnPlayerFallHandler;
        GameEvents.Instance.OnPlayerStepped -= OnPlayerSteppedHandler;
        // GameEvents.Instance.OnShadowTrapActivated -= OnShadowTrapActivatedHandler;
        // GameEvents.Instance.OnSpikesTrapActivated -= OnSpikesTrapActivatedHandler;
        GameEvents.Instance.OnPlayerDestroyed -= OnPlayerDestroyedHandler;
    }

    private void OnPlayerDestroyedHandler()
    {
        _commonDeathSound.Play();
        _exhaleSound.Play();
    }

    private void OnPlayerSteppedHandler()
    {
        SetRandomPitch(_stepSound, minStepSoundPitch, maxStepSoundPitch);
        _stepSound.Play();
    }

    private void OnPlayerFallHandler()
    {
        _fallDeathSound.Play();
    }

    // private void OnShadowTrapActivatedHandler()
    // {
    //     _shadowDeathSound.Play();
    // }

    private void SetRandomPitch(AudioSource audioSource, float minPitch, float maxPitch)
    {
        audioSource.pitch = UnityEngine.Random.Range(minPitch, maxPitch);
    }

}
