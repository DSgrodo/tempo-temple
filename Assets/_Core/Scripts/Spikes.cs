using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    private const string hideSpikesAnimationTrigger = "HideSpikes";
    private const string showSpikesAnimationTrigger = "ShowSpikes";
    [SerializeField] private TagsContainer _tags;
    [SerializeField] private bool _isStatic = false;
    [SerializeField] private float _activeTime = 1f;
    [SerializeField] private float _inactiveTime = 1f;
    [SerializeField] private float _countedTime = 0f;
    [SerializeField] private AudioSource _spikesIn;
    [SerializeField] private AudioSource _spikesOut;
    private bool _isActive = false;
    private Animator _animator;

    private void Awake()
    {
        InitAnimator();
    }

    private void Update()
    {
        if (_isStatic == false)
        {
            HandleOnOffLoop();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == _tags.Player)
        {
            GameEvents.Instance.SpikesTrapActivated();
        }
    }


    private void InitAnimator()
    {
        _animator = GetComponent<Animator>();
    }

    private void HandleOnOffLoop()
    {
        if (_isActive == true)
        {
            if (_countedTime >= _activeTime)
            {
                _isActive = false;
                _spikesIn.Play();
                _animator.SetTrigger(hideSpikesAnimationTrigger);
                _countedTime = 0f;
            }
            _countedTime += Time.deltaTime;
        }
        else
        {
            if (_countedTime >= _inactiveTime)
            {
                _isActive = true;
                _spikesOut.Play();
                _animator.SetTrigger(showSpikesAnimationTrigger);
                _countedTime = 0f;
            }
            _countedTime += Time.deltaTime;
        }
    }
}

