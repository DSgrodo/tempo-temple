using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOtherObjectOnEnable : MonoBehaviour
{
    [SerializeField] private GameObject _otherObject;

    private void OnEnable()
    {
        _otherObject.SetActive(true);
    }
}
