using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallTrap : MonoBehaviour
{
    [SerializeField] private TagsContainer _tags;

    private void OnTriggerEnter(Collider other)
    {
        HandlePlayerCollider(other);
    }

    private void HandlePlayerCollider(Collider other)
    {
        if (other.tag == _tags.Player)
        {
            GameEvents.Instance.FireBallTrapActivated();
        }
    }
}
