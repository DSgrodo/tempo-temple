using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDpadController : MonoBehaviour
{
    [SerializeField] private PlayerController _playerController;
    public void SetMoveDirection(int directionIndex)
    {
        if (PlayerController.IsOnMovingPlatform == false)
        {
            Vector3 newMoveDirection = Vector3.zero;
            switch (directionIndex)
            {
                case 0:
                    newMoveDirection = Vector3.forward;
                    break;
                case 1:
                    newMoveDirection = Vector3.right;
                    break;
                case 2:
                    newMoveDirection = Vector3.back;
                    break;
                case 3:
                    newMoveDirection = Vector3.left;
                    break;
                default:
                    break;
            }
            _playerController.MoveDirection = newMoveDirection;
        }
    }
}
