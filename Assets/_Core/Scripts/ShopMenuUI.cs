using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class ShopMenuUI : MonoBehaviour
{
    [SerializeField] private TMP_Text _coins;
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;

    private void Start()
    {
        SubscribeToEvents();
        UpdateCoinsAmountText();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }


    private void UpdateCoinsAmountText()
    {
        _coins.text = PlayerPrefs.GetInt(_playerPrefsKeys.CoinsAmount).ToString();
    }

    private void OnCoinsUpdateHandler()
    {
        UpdateCoinsAmountText();
    }

    private void SubscribeToEvents()
    {
        GameEvents.Instance.OnCoinsUpdate += OnCoinsUpdateHandler;
    }

    private void UnsubscribeFromEvents()
    {
        GameEvents.Instance.OnCoinsUpdate -= OnCoinsUpdateHandler;
    }


}
