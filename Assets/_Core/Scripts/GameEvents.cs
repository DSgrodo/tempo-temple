using System;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents Instance { get; private set; }

    public event Action OnShadowTrapActivated;
    public event Action OnPlayerFall;
    public event Action OnPlayerDestroyed;
    public event Action OnPickedUpCoin;
    public event Action OnPlayerStepped;
    public event Action OnSpikesTrapActivated;
    public event Action OnTeleporterTrapActivated;
    public event Action OnFireBallTrapActivated;

    public event Action OnCoinsUpdate;
    public event Action OnSkinUpdate;


    private void Awake()
    {
        InitSingleton();
    }


    private void InitSingleton()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void SkinUpdate()
    {
        OnSkinUpdate?.Invoke();
    }

    public void CoinsUpdate()
    {
        OnCoinsUpdate?.Invoke();
    }

    public void FireBallTrapActivated()
    {
        OnFireBallTrapActivated?.Invoke();
    }

    public void ShadowTrapActivated()
    {
        OnShadowTrapActivated?.Invoke();
    }

    public void TeleporterTrapActivated()
    {
        OnTeleporterTrapActivated?.Invoke();
    }

    public void SpikesTrapActivated()
    {
        OnSpikesTrapActivated?.Invoke();
    }

    public void PlayerFall()
    {
        OnPlayerFall?.Invoke();
    }

    public void PlayerDestroyed()
    {
        OnPlayerDestroyed?.Invoke();
    }

    public void PickedUpCoin()
    {
        OnPickedUpCoin?.Invoke();
    }

    public void PlayerStepped()
    {
        OnPlayerStepped?.Invoke();
    }
}
