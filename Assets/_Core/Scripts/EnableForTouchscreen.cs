using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class EnableForTouchscreen : MonoBehaviour
{
    private void Awake()
    {
        if (InputSystem.GetDevice<Touchscreen>() != null)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
