using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectMovement : MonoBehaviour
{
    [SerializeField] private Transform _pointBObject;
    [SerializeField] private float _moveSpeed = 1f;
    [SerializeField] private float _waitingTime = 0f;
    [SerializeField] private bool _isLooping = false;
    private float _distance;
    private Vector3 _pointA;
    private Vector3 _pointB;
    private Vector3 _currentTargetPoint;

    public bool IsMoving { get; set; } = true;

    private void OnEnable()
    {
        InitVariables();
        StartCoroutine(Move(_pointB));
    }

    private void InitVariables()
    {
        _pointA = transform.position;
        _pointB = _pointBObject.transform.position;
        _distance = Vector3.Distance(_pointA, _pointB);
    }

    public void ResetSelf()
    {
        IsMoving = true;
        _currentTargetPoint = _pointB;
    }

    private IEnumerator Move(Vector3 targetPosition)
    {
        Vector3 startPosition = transform.position;
        Vector3 direction = Vector3.Normalize(targetPosition - startPosition);
        Vector3 newPosition;

        while (NotAtTargetPosition(startPosition))
        {
            IsMoving = true;
            newPosition = CalculateNewPosition(direction);
            if (NewPositionIsTooFar(startPosition, newPosition))
            {
                SetPositionToTarget(targetPosition);
                IsMoving = false;
                if (_isLooping == true)
                {
                    if (_currentTargetPoint == _pointA)
                    {
                        _currentTargetPoint = _pointB;
                    }
                    else
                    {
                        _currentTargetPoint = _pointA;
                    }
                    yield return new WaitForSeconds(_waitingTime);
                    StartCoroutine(Move(_currentTargetPoint));
                }
                yield break;
            }
            else
            {
                MoveToNewPosition(newPosition);
                yield return null;
            }
        }

        bool NewPositionIsTooFar(Vector3 startPosition, Vector3 newPosition)
        {
            return Vector3.Distance(startPosition, newPosition) > _distance;
        }

        void SetPositionToTarget(Vector3 targetPosition)
        {
            transform.position = targetPosition;
        }

        void MoveToNewPosition(Vector3 newPosition)
        {
            transform.position = newPosition;
        }

        bool NotAtTargetPosition(Vector3 startPosition)
        {
            return Vector3.Distance(startPosition, transform.position) < _distance;
        }

        Vector3 CalculateNewPosition(Vector3 direction)
        {
            return transform.position + direction * _moveSpeed * Time.deltaTime;
        }
    }
}
