using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakePlayerChild : MonoBehaviour
{
    [SerializeField] private TagsContainer _tags;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == _tags.Player)
        {
            other.collider.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == _tags.Player)
        {
            other.collider.transform.SetParent(null);
        }
    }
}
