using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameMenu : MonoBehaviour
{
    private const string MenuLevel = "Menu";
    [SerializeField] private GameObject _endGameUI;

    private void Start()
    {
        SubscribeToEvents();
    }


    private void SubscribeToEvents()
    {
        GameEvents.Instance.OnPlayerDestroyed += OnPlayerDestroyedHandler;
    }

    private void UnsubscribeFromEvents()
    {
        GameEvents.Instance.OnPlayerDestroyed -= OnPlayerDestroyedHandler;
    }

    private void OnPlayerDestroyedHandler()
    {
        if (_endGameUI != null)
        {
            _endGameUI.SetActive(true);
        }
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(MenuLevel);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
