using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterTrap : MonoBehaviour
{
    [SerializeField] private Transform _targetTeleportPoint;
    [SerializeField] private AudioSource _audioSource;

    private void OnTriggerEnter(Collider other)
    {
        GameEvents.Instance.TeleporterTrapActivated();
        Teleport(other);
        _audioSource.Play();
    }

    private void Teleport(Collider other)
    {
        other.transform.position = new Vector3(_targetTeleportPoint.position.x, other.transform.position.y, _targetTeleportPoint.position.z);
    }
}
