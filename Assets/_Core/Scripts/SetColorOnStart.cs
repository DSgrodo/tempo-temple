using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetColorOnStart : MonoBehaviour
{
    [SerializeField] private Color _color;
    [SerializeField] private List<GameObject> _targetObjects;

    private void Start()
    {
        SetColor();
    }

    private void SetColor()
    {
        foreach (GameObject targetObject in _targetObjects)
        {
            targetObject.GetComponent<Renderer>().material.SetColor("_Color", _color);
        }
    }
}
