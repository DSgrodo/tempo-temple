using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkinShopItemUI : MonoBehaviour
{
    [SerializeField] private TMP_Text _name;
    [SerializeField] private TMP_Text _price;
    [SerializeField] private Image _image;
    [SerializeField] private GameObject _outline;

    [SerializeField] private Button _useButton;
    [SerializeField] private GameObject _buyButton;

    public void Setup(string name, int price, Sprite sprite, int hasAccessToSkin)
    {
        _name.text = name;
        _price.text = price.ToString();
        _image.sprite = sprite;

        if (hasAccessToSkin == 1)
        {
            _buyButton.SetActive(false);
        }
        else
        {
            _useButton.interactable = false;
        }
    }

    public void TurnOnUseButton()
    {
        _useButton.interactable = true;
    }

    public void RemoveBuyButton()
    {
        _buyButton.SetActive(false);
    }

    public void AddOutline()
    {
        _outline.SetActive(true);
    }

    public void RemoveOutline()
    {
        _outline.SetActive(false);
    }
}
