using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudioPlayer : MonoBehaviour
{
    [SerializeField] private AudioSource _buttonEnter;
    [SerializeField] private AudioSource _buttonExit;

    public static UIAudioPlayer Instance { get; private set; }

    private void Awake()
    {
        InitSingleton();
    }

    private void InitSingleton()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void PlayButtonEnter()
    {
        _buttonEnter.Play();
    }

    public void PlayButtonExit()
    {
        _buttonExit.Play();
    }

}
