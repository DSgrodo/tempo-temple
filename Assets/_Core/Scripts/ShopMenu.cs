using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopMenu : MonoBehaviour
{
    [SerializeField] private SkinsContainer _skins;
    [SerializeField] private GameObject _skinShopItem;
    [SerializeField] private GameObject _skinsShopItemContainer;
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;

    private void Start()
    {
        HandleSkinsList();
    }

    private void HandleSkinsList()
    {
        for (int index = 0; index < _skins.list.Count; index++)
        {
            if (_skins.list[index].availableOnStart)
            {
                PlayerPrefs.SetInt(_playerPrefsKeys.HasAccessToSkin + index, 1);
            }
            CreateSkinShopItem(index);
        }
    }

    private void CreateSkinShopItem(int index)
    {
        GameObject instance = Instantiate(_skinShopItem);
        instance.transform.SetParent(_skinsShopItemContainer.transform);
        instance.transform.localScale = Vector3.one;
        instance.GetComponent<SkinShopItemUI>().Setup(_skins.list[index].name, _skins.list[index].price, _skins.list[index].sprite, PlayerPrefs.GetInt(_playerPrefsKeys.HasAccessToSkin + index));
        instance.GetComponent<SkinShopItem>().Setup(index, _skins.list[index].price);
    }
}
