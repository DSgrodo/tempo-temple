using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTilt : MonoBehaviour
{
    private const float tiltAngle = 10f;
    [SerializeField] private PlayerController _playerController;

    void Update()
    {
        HandlePlayerTilt();
    }


    private void HandlePlayerTilt()
    {
        transform.localRotation = _playerController.IsMoving ? Quaternion.Euler(tiltAngle, 0f, 0f) : Quaternion.Euler(0f, 0f, 0f);
    }
}
