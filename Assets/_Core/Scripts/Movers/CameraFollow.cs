using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _smoothTime = 5f;
    private Vector3 _cameraOffset;
    private Vector3 _desiredCameraPosition;
    private Vector3 _smoothedCameraPosition;
    private Vector3 _velocity = Vector3.zero;
    private Vector3 _lastTargetPosition;

    private void Start()
    {
        SubscribeToEvents();
        CalculateCameraOffset(_target.position);
    }

    private void LateUpdate()
    {
        HandleMovement();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }


    private void HandleMovement()
    {
        if (_target != null)
        {
            MoveCamera(_target.position);
            _lastTargetPosition = _target.position;
        }
        else
        {
            MoveCamera(_lastTargetPosition);
        }
    }

    private void SubscribeToEvents()
    {
        GameEvents.Instance.OnPlayerFall += OnPlayerFallHandler;
    }

    private void UnsubscribeFromEvents()
    {
        GameEvents.Instance.OnPlayerFall -= OnPlayerFallHandler;
    }

    private void OnPlayerFallHandler()
    {
        DetachFromTarget();
    }

    private void DetachFromTarget()
    {
        _target = null;
    }

    private void MoveCamera(Vector3 position)
    {
        _desiredCameraPosition = position + _cameraOffset;
        _smoothedCameraPosition = Vector3.SmoothDamp(transform.position, _desiredCameraPosition, ref _velocity, _smoothTime);
        transform.position = _smoothedCameraPosition;
    }

    private void CalculateCameraOffset(Vector3 position)
    {
        _cameraOffset = transform.position - position;
    }
}
