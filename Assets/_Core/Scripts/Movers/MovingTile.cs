using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTile : MonoBehaviour
{
    [SerializeField] private TagsContainer _tags;
    [SerializeField] private AudioSource _startedMovingSound;
    [SerializeField] private AudioSource _finishedMovingSound;
    private bool _isAtTargetPosition = false;
    private DirectMovement _directMovement;
    private PlayerController _playerController;
    private Vector3 _defaultPosition;

    private void Awake()
    {
        InitDirectMovement();
    }

    private void OnEnable()
    {
        InitDefaultPosition();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == _tags.Player && _isAtTargetPosition == false)
        {
            InitPlayerComponents(other);
        }
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == _tags.Player && _isAtTargetPosition == false)
        {
            if (PlayerNotMoving() && TileNotMoving())
            {
                StartMovingTile();
                PlayerController.IsOnMovingPlatform = true;
                _startedMovingSound.Play();
            }

            if (FinishedMovingTile())
            {
                PlayerController.IsOnMovingPlatform = false;
                _startedMovingSound.Stop();
                _finishedMovingSound.Play();
                _isAtTargetPosition = true;
            }
        }
    }

    private void OnDisable()
    {
        ResetSelf();
    }


    private bool TileNotMoving()
    {
        return _directMovement.enabled == false;
    }

    private bool FinishedMovingTile()
    {
        return _directMovement.IsMoving == false;
    }

    private bool PlayerNotMoving()
    {
        return _playerController.IsMoving == false;
    }

    private void StartMovingTile()
    {
        _directMovement.enabled = true;
    }

    private void InitPlayerComponents(Collision other)
    {
        _playerController = other.gameObject.GetComponent<PlayerController>();
    }


    private void ResetSelf()
    {
        transform.position = _defaultPosition;
        _isAtTargetPosition = false;
        _directMovement.ResetSelf();
        _directMovement.enabled = false;
    }

    private void InitDirectMovement()
    {
        _directMovement = GetComponent<DirectMovement>();
    }

    private void InitDefaultPosition()
    {
        _defaultPosition = transform.position;
    }
}
