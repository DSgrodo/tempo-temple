using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowInstantly : MonoBehaviour
{
    [SerializeField] private Transform _target;

    void Update()
    {
        Follow();
    }

    private void Follow()
    {
        if (_target != null)
        {
            transform.position = _target.transform.position;
        }
    }
}
