using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PingPongLinearMovement : MonoBehaviour
{
    [SerializeField] private Transform _targetPoint;
    [SerializeField] private float _timeToMove = 5f;
    void Start()
    {
        StartMovingLoop();
    }

    private void StartMovingLoop()
    {
        transform.DOMove(_targetPoint.position, _timeToMove)
            .SetEase(Ease.InOutFlash)
            .SetLoops(-1, LoopType.Yoyo);
    }
}
