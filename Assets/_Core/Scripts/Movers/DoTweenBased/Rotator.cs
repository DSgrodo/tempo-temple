using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Rotator : MonoBehaviour
{
    [SerializeField] private float _FullRotationTime = 5f;
    void Start()
    {
        StartConstantRotation();
    }

    private void StartConstantRotation()
    {
        transform.DORotate(new Vector3(0f, 360f, 0f), _FullRotationTime, RotateMode.FastBeyond360)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Incremental);
    }
}
