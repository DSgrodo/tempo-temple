using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelItem : MonoBehaviour
{
    private const string LevelNameBase = "level";
    private int _index;

    public void Setup(int index)
    {
        _index = index;
    }

    public void LoadLevel()
    {
        SceneLoader.Instance.LoadScene(LevelNameBase + _index);
    }
}
