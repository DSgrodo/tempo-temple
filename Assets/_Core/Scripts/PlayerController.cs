using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public static bool IsOnMovingPlatform { get; set; } = false;

    [SerializeField] private float _moveDistance = 1f;
    [SerializeField] private float _moveSpeed = 5f;
    [SerializeField] private float yDeathCoordinate = -10f;
    [SerializeField] private LayerMask _floor;
    [SerializeField] private GameObject _newRootObject;
    [SerializeField] private Animator _animator;
    [SerializeField] private PlayerTilt _playerTilt;

    public Vector3 MoveDirection { get; set; } = Vector3.zero;
    public bool IsMoving { get; set; } = false;

    private Rigidbody _rigidbody;


    private void Awake()
    {
        InitRigidbody();
    }

    private void Start()
    {
        SubscribeToEvents();
        ResetFlags();
    }

    private void Update()
    {
        HandleMovement();
        YCoordinateCheck();
    }

    private void OnDestroy()
    {
        GameEvents.Instance.PlayerDestroyed();
        UnsubscribeFromEvents();
    }


    private void UnsubscribeFromEvents()
    {
        GameEvents.Instance.OnPlayerFall -= OnPlayerFallHandler;
        GameEvents.Instance.OnShadowTrapActivated -= OnShadowTrapActivatedHandler;
        GameEvents.Instance.OnSpikesTrapActivated -= OnSpikesTrapActivatedHandler;
        GameEvents.Instance.OnTeleporterTrapActivated -= OnTeleporterTrapActivatedHandler;
        GameEvents.Instance.OnFireBallTrapActivated -= OnFireBallTrapActivatedHandler;
    }

    private void SubscribeToEvents()
    {
        GameEvents.Instance.OnPlayerFall += OnPlayerFallHandler;
        GameEvents.Instance.OnShadowTrapActivated += OnShadowTrapActivatedHandler;
        GameEvents.Instance.OnSpikesTrapActivated += OnSpikesTrapActivatedHandler;
        GameEvents.Instance.OnTeleporterTrapActivated += OnTeleporterTrapActivatedHandler;
        GameEvents.Instance.OnFireBallTrapActivated += OnFireBallTrapActivatedHandler;
    }

    private void ResetFlags()
    {
        IsOnMovingPlatform = false;
    }

    private void OnTeleporterTrapActivatedHandler()
    {
        StopCoroutine(nameof(ChangePosition));
        IsMoving = false;
    }

    private void YCoordinateCheck()
    {
        if (transform.position.y < yDeathCoordinate)
        {
            Destroy(gameObject);
        }
    }

    private void OnShadowTrapActivatedHandler()
    {
        Destroy(gameObject);
    }
    private void OnSpikesTrapActivatedHandler()
    {
        Destroy(gameObject);
    }

    private void OnFireBallTrapActivatedHandler()
    {
        Destroy(gameObject);
    }

    private void OnPlayerFallHandler()
    {
        _playerTilt.enabled = false;
        IsMoving = true;
        transform.parent = Instantiate(_newRootObject, transform.position, Quaternion.identity).transform;
        _animator.enabled = true;
        // _rigidbody.useGravity = true;
    }

    private void HandleMovement()
    {
        if (IsMoving == false && IsOnMovingPlatform == false)
        {
            if (StandingOnFloor())
            {
                CheckInput();
            }
            else
            {
                GameEvents.Instance.PlayerFall();
            }
        }
    }

    private void InitRigidbody()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private bool StandingOnFloor()
    {
        return Physics.Raycast(transform.position, Vector3.down, _moveDistance, _floor);
    }

    private void CheckInput()
    {
        if (MoveDirection != Vector3.zero && (MoveDirection == Vector3.forward
                                              || MoveDirection == Vector3.back
                                              || MoveDirection == Vector3.right
                                              || MoveDirection == Vector3.left))
        {
            Move(MoveDirection);
            MoveDirection = Vector3.zero;
        }
    }

    public void MoveInputHandler(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            MoveDirection = new Vector3(context.ReadValue<Vector2>().x, 0f, context.ReadValue<Vector2>().y);
        }
    }

    private void Move(Vector3 direction)
    {
        GameEvents.Instance.PlayerStepped();
        SetRotation(direction);
        if (!Physics.Raycast(transform.position, direction, _moveDistance, _floor))
        {
            StartCoroutine(nameof(ChangePosition), direction);
        }
    }



    private void SetRotation(Vector3 direction)
    {
        transform.rotation = Quaternion.LookRotation(direction);
    }

    private IEnumerator ChangePosition(Vector3 direction)
    {
        Vector3 startPosition = transform.position;
        Vector3 targetPosition = startPosition + direction * _moveDistance;
        Vector3 newPosition;

        IsMoving = true;

        while (NotAtTargetPosition(startPosition))
        {
            newPosition = CalculateNewPosition(direction);
            if (NewPositionIsTooFar(startPosition, newPosition))
            {
                SetPositionToTarget(targetPosition);
                IsMoving = false;
                yield break;
            }
            else
            {
                MoveToNewPosition(newPosition);
                yield return null;
            }
        }
        IsMoving = false;

        bool NewPositionIsTooFar(Vector3 startPosition, Vector3 newPosition)
        {
            return Vector3.Distance(startPosition, newPosition) > _moveDistance;
        }

        void SetPositionToTarget(Vector3 targetPosition)
        {
            transform.position = targetPosition;
        }

        void MoveToNewPosition(Vector3 newPosition)
        {
            transform.position = newPosition;
        }

        bool NotAtTargetPosition(Vector3 startPosition)
        {
            return Vector3.Distance(startPosition, transform.position) < _moveDistance;
        }

        Vector3 CalculateNewPosition(Vector3 direction)
        {
            return transform.position + direction * _moveSpeed * Time.deltaTime;
        }
    }

}
