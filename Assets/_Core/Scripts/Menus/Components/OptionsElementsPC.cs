using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionsElementsPC : MonoBehaviour
{
    [SerializeField] public Toggle isFullscreen;
    [SerializeField] public TMP_Dropdown resolution;
    [SerializeField] public TMP_Dropdown graphicsLevel;
    [SerializeField] public Slider generalVolume;
    [SerializeField] public Slider musicVolume;
    [SerializeField] public Slider effectsVolume;
}
