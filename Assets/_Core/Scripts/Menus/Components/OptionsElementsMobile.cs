using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionsElementsMobile : MonoBehaviour
{
    [SerializeField] public TMP_Dropdown controlsType;
    [SerializeField] public TMP_Dropdown graphicsLevel;
    [SerializeField] public Slider generalVolume;
    [SerializeField] public Slider musicVolume;
    [SerializeField] public Slider effectsVolume;
}
