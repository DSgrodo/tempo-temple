using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMixerParameterChanger : MonoBehaviour
{
    private const int coefficient = 20;
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private string _exposedParameterName;

    public void SetExposedParameter(float newVolume)
    {
        _audioMixer.SetFloat(_exposedParameterName, Mathf.Log10(newVolume) * coefficient);
    }
}
