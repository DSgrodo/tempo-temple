using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class PauseMenu : MonoBehaviour
{
    private const string TutorialBackgroundMusic = "Music";
    private const string MenuLevel = "Menu";
    [SerializeField] private GameObject _pauseMenuUI;
    [SerializeField] private PlayerInput _playerInput;
    public static bool GameIsPaused = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }
    }

    private void OnDestroy()
    {
        Resume();
    }

    public void TogglePauseMenu()
    {
        if (GameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    public void Resume()
    {
        GameIsPaused = false;
        Time.timeScale = 1f;
        _pauseMenuUI.SetActive(false);
        if (_playerInput != null)
        {
            _playerInput.enabled = true;
        }
    }

    private void Pause()
    {
        GameIsPaused = true;
        Time.timeScale = 0f;
        _pauseMenuUI.SetActive(true);
        if (_playerInput != null)
        {
            _playerInput.enabled = false;
        }
    }

    public void LoadMainMenu()
    {
        // AudioManager.Instance.Stop(TutorialBackgroundMusic);
        SceneManager.LoadScene(MenuLevel);
        Time.timeScale = 1f;
    }

    public void Quit()
    {
        Application.Quit();
    }


}
