using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenuActivator : MonoBehaviour
{
    [SerializeField] private GameObject _optionsMenuPC;
    [SerializeField] private GameObject _optionsMenuMobile;

    public void ShowOptionsMenu()
    {
#if UNITY_ANDROID || UNITY_IOS
        _optionsMenuMobile.SetActive(true);
#else
        _optionsMenuPC.SetActive(true);
#endif
    }

}
