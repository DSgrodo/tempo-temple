using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;

public class OptionsMenuPC : MonoBehaviour
{
    [SerializeField] private OptionsElementsPC _optionsElements;
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;
    private List<Resolution> _resolutionsWidthAndHeightOnly = new List<Resolution>();

    private void Start()
    {
        InitResolutions();

        if (PlayerPrefs.HasKey(_playerPrefsKeys.IsFullscreen))
        {
            LoadSettingsFromPlayerPrefs();
        }
    }


    // PlayerPrefs dependent
    private void LoadSettingsFromPlayerPrefs()
    {
        if (PlayerPrefs.GetInt(_playerPrefsKeys.IsFullscreen) == 1)
        {
            _optionsElements.isFullscreen.isOn = true;
        }
        else
        {
            _optionsElements.isFullscreen.isOn = false;
        }

        _optionsElements.resolution.value = PlayerPrefs.GetInt(_playerPrefsKeys.ResolutionIndex);
        _optionsElements.graphicsLevel.value = PlayerPrefs.GetInt(_playerPrefsKeys.GraphicsLevelIndex);

        _optionsElements.generalVolume.value = PlayerPrefs.GetFloat(_playerPrefsKeys.GeneralVolume);
        _optionsElements.musicVolume.value = PlayerPrefs.GetFloat(_playerPrefsKeys.MusicVolume);
        _optionsElements.effectsVolume.value = PlayerPrefs.GetFloat(_playerPrefsKeys.EffectsVolume);
    }

    public void SaveSettingsToPlayerPrefs()
    {
        if (_optionsElements.isFullscreen.isOn == true)
        {
            PlayerPrefs.SetInt(_playerPrefsKeys.IsFullscreen, 1);
        }
        else
        {
            PlayerPrefs.SetInt(_playerPrefsKeys.IsFullscreen, 0);
        }

        PlayerPrefs.SetInt(_playerPrefsKeys.ResolutionIndex, _optionsElements.resolution.value);
        PlayerPrefs.SetInt(_playerPrefsKeys.GraphicsLevelIndex, _optionsElements.graphicsLevel.value);

        PlayerPrefs.SetFloat(_playerPrefsKeys.GeneralVolume, _optionsElements.generalVolume.value);
        PlayerPrefs.SetFloat(_playerPrefsKeys.MusicVolume, _optionsElements.musicVolume.value);
        PlayerPrefs.SetFloat(_playerPrefsKeys.EffectsVolume, _optionsElements.effectsVolume.value);
    }


    // InitResolutions and it's components
    private void InitResolutions()
    {
        Resolution[] resolutionsRaw;
        List<string> options = new List<string>();
        int currentResolutionIndex = 0;

        _optionsElements.resolution.ClearOptions();
        resolutionsRaw = Screen.resolutions;
        SortUniqueResolutionsOnly(resolutionsRaw);
        currentResolutionIndex = AddResolutionsToStringsList(options, currentResolutionIndex);
        AddResolutionsToDropdown(options, currentResolutionIndex);
    }

    private void AddResolutionsToDropdown(List<string> options, int currentResolutionIndex)
    {
        _optionsElements.resolution.AddOptions(options);
        _optionsElements.resolution.value = currentResolutionIndex;
        _optionsElements.resolution.RefreshShownValue();
    }

    private int AddResolutionsToStringsList(List<string> options, int currentResolutionIndex)
    {
        for (int i = 0; i < _resolutionsWidthAndHeightOnly.Count; i++)
        {
            options.Add(_resolutionsWidthAndHeightOnly[i].width.ToString() + " x " + _resolutionsWidthAndHeightOnly[i].height.ToString());
            if (_resolutionsWidthAndHeightOnly[i].width == Screen.width && _resolutionsWidthAndHeightOnly[i].height == Screen.height)
                currentResolutionIndex = i;
        }

        return currentResolutionIndex;
    }

    private void SortUniqueResolutionsOnly(Resolution[] resolutionsRaw)
    {
        // Making a list of resolutions (Width&Height only, without refreshRate)
        for (int i = 0; i < resolutionsRaw.Length; i++)
        {
            if (i > 0)
            {
                if (resolutionsRaw[i].width == resolutionsRaw[i - 1].width && resolutionsRaw[i].height == resolutionsRaw[i - 1].height)
                    continue;
            }
            _resolutionsWidthAndHeightOnly.Add(resolutionsRaw[i]);
        }
    }


    // Called by UI elements
    public void SetResolution(int resolutionIndex)
    {
        Screen.SetResolution(_resolutionsWidthAndHeightOnly[resolutionIndex].width, _resolutionsWidthAndHeightOnly[resolutionIndex].height, Screen.fullScreen);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

}
