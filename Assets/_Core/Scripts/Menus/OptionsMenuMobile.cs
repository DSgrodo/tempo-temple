using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;

public class OptionsMenuMobile : MonoBehaviour
{
    [SerializeField] private OptionsElementsMobile _optionsElements;
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;

    private void Start()
    {
        if (PlayerPrefs.HasKey(_playerPrefsKeys.GeneralVolume))
        {
            LoadSettingsFromPlayerPrefs();
        }
    }


    private void LoadSettingsFromPlayerPrefs()
    {
        _optionsElements.controlsType.value = PlayerPrefs.GetInt(_playerPrefsKeys.ControlsType);
        _optionsElements.graphicsLevel.value = PlayerPrefs.GetInt(_playerPrefsKeys.GraphicsLevelIndex);

        _optionsElements.generalVolume.value = PlayerPrefs.GetFloat(_playerPrefsKeys.GeneralVolume);
        _optionsElements.musicVolume.value = PlayerPrefs.GetFloat(_playerPrefsKeys.MusicVolume);
        _optionsElements.effectsVolume.value = PlayerPrefs.GetFloat(_playerPrefsKeys.EffectsVolume);
    }

    public void SaveSettingsToPlayerPrefs()
    {
        PlayerPrefs.SetInt(_playerPrefsKeys.ControlsType, _optionsElements.controlsType.value);
        PlayerPrefs.SetInt(_playerPrefsKeys.GraphicsLevelIndex, _optionsElements.graphicsLevel.value);

        PlayerPrefs.SetFloat(_playerPrefsKeys.GeneralVolume, _optionsElements.generalVolume.value);
        PlayerPrefs.SetFloat(_playerPrefsKeys.MusicVolume, _optionsElements.musicVolume.value);
        PlayerPrefs.SetFloat(_playerPrefsKeys.EffectsVolume, _optionsElements.effectsVolume.value);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }
}
