
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    private const string mainLevel = "Main";
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;
    [SerializeField] private TMP_Text _record;

    private void Start()
    {
        SetRecordText();
    }

    private void SetRecordText()
    {
        _record.text = PlayerPrefs.GetInt(_playerPrefsKeys.Record).ToString();
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(mainLevel);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
