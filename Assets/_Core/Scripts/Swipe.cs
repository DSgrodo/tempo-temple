using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Swipe : MonoBehaviour
{
    [SerializeField] private PlayerController _playerController;
    private bool _startPositionIsSet = false;
    private bool _isTouching = false;
    private Vector2 _startPosition;
    private Vector2 _endPosition;
    private float _timeCounter;
    private float _maxSwipeTime = 1f;
    private float _minSwipeDistance = 100f;

    private void Update()
    {
        CheckSwipeParameters();
    }


    private void CheckSwipeParameters()
    {
        if (_isTouching == true)
        {
            _timeCounter += Time.deltaTime;
            if (_timeCounter > _maxSwipeTime)
            {
                ResetFlags();
            }
        }
    }

    private void ResetFlags()
    {
        _startPositionIsSet = false;
        _isTouching = false;
    }


    public void PressureInputHandler(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            bool value = context.ReadValueAsButton();

            if (_isTouching == false && value == true) // SwipeStarted
            {
                _timeCounter = 0f;
            }

            if (_isTouching == true && value == false) // SwipeEnded
            {
                HandleSwipe(_startPosition, _endPosition);
                ResetFlags();
            }

            _isTouching = value;
        }
    }

    public void PositionInputHandler(InputAction.CallbackContext context)
    {
        Vector2 value = context.ReadValue<Vector2>();

        if (_isTouching == true && _startPositionIsSet == false)
        {
            _startPosition = value;
            _endPosition = value;
            _startPositionIsSet = true;
        }
        else if (_isTouching == true)
        {
            _endPosition = value;
        }
    }

    private void HandleSwipe(Vector2 startPosition, Vector2 endPosition)
    {
        float deltaX = endPosition.x - startPosition.x;
        float deltaY = endPosition.y - startPosition.y;
        if (Vector2.Distance(startPosition, endPosition) >= _minSwipeDistance)
        {
            if (Mathf.Abs(deltaX) > Mathf.Abs(deltaY))
            {
                if (deltaX > 0f)
                {
                    // Debug.Log("Swipe RIGHT with deltaX = " + deltaX);
                    _playerController.MoveDirection = Vector3.right;
                }
                else
                {
                    // Debug.Log("Swipe LEFT with deltaX = " + deltaX);
                    _playerController.MoveDirection = Vector3.left;
                }
            }
            else
            {
                if (deltaY > 0f)
                {
                    // Debug.Log("Swipe UP with deltaY = " + deltaY);
                    _playerController.MoveDirection = Vector3.forward;
                }
                else
                {
                    // Debug.Log("Swipe DOWN with deltaY = " + deltaY);
                    _playerController.MoveDirection = Vector3.back;
                }
            }
        }
    }
}
