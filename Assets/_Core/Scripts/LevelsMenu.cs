using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelsMenu : MonoBehaviour
{
    [SerializeField] private int _numberOfLevels = 54;
    [SerializeField] private int _numberOfLevelsOnPage = 16;

    [SerializeField] private LevelsMenuUI _levelsMenuUI;
    [SerializeField] private GameObject _levelItemUIPrefab;
    [SerializeField] private GameObject _levelsGrid;
    private int _lastLevelItemIndex;
    private int _itemsAmountOnCurrentPage = 0;

    private void OnEnable()
    {
        ClearList();
        LoadLevelsList(1);
    }


    private void LoadLevelsList(int firstItemIndex)
    {
        CreateAllLevelItems(firstItemIndex);
        HandleButtonsState();
    }

    private void CreateAllLevelItems(int firstItemIndex)
    {
        int i = firstItemIndex;
        _itemsAmountOnCurrentPage = 0;
        while ((_itemsAmountOnCurrentPage < _numberOfLevelsOnPage) && (i <= _numberOfLevels))
        {
            CreateLevelItem(i);
            _lastLevelItemIndex = i;
            _itemsAmountOnCurrentPage++;
            i++;
        }
    }

    private void HandleButtonsState()
    {
        _levelsMenuUI.buttonLeft.SetActive(!(_lastLevelItemIndex <= _numberOfLevelsOnPage));
        _levelsMenuUI.buttonRight.SetActive(!(_lastLevelItemIndex == _numberOfLevels));
    }

    private void CreateLevelItem(int index)
    {
        GameObject instance = Instantiate(_levelItemUIPrefab);
        instance.transform.SetParent(_levelsGrid.transform);
        instance.transform.localScale = Vector3.one;
        instance.GetComponent<LevelItemUI>().Setup(index);
        instance.GetComponent<LevelItem>().Setup(index);
    }

    public void LoadNextPage()
    {
        ClearList();
        LoadLevelsList(_lastLevelItemIndex + 1);
    }

    public void LoadPreviousPage()
    {
        ClearList();
        LoadLevelsList(_lastLevelItemIndex - _itemsAmountOnCurrentPage - _numberOfLevelsOnPage + 1);
    }

    private void ClearList()
    {
        foreach (Transform child in _levelsGrid.transform)
        {
            Destroy(child.gameObject);
        }
    }

}
