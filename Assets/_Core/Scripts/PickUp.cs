using UnityEngine;

public class PickUp : MonoBehaviour
{
    [SerializeField] protected TagsContainer tagsContainer;
    protected bool _isTriggered = false;

    private void OnEnable()
    {
        ResetTrigger();
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(tagsContainer.Player) && _isTriggered == false)
        {
            _isTriggered = true;
            PickedUp(other);
        }
    }


    private void ResetTrigger()
    {
        _isTriggered = false;
    }

    virtual protected void PickedUp(Collider player)
    {
        Destroy(gameObject);
    }

}