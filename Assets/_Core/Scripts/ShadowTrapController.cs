using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowTrapController : MonoBehaviour
{
    [SerializeField] private TagsContainer _tags;
    [SerializeField] private Transform _target;
    [SerializeField] private float _speed = 2f;
    [SerializeField] private float _maxDistanceToTarget = 20f;
    [SerializeField] private float _ParallelMovementSmoothTime = 0.5f;
    private bool _isTargetReached = false;
    private Vector3 _velocity = Vector3.zero;

    private void Start()
    {
        ResetPosition();
    }

    private void Update()
    {
        if (_isTargetReached == false && _target != null)
        {
            MoveToTarget();
        }
    }


    private void ResetPosition()
    {
        transform.position = new Vector3(_target.position.x, transform.position.y, _target.position.z - _maxDistanceToTarget);
    }

    private void MoveToTarget()
    {
        float zDistanceToTarget = Mathf.Abs((_target.position - transform.position).z);
        if (zDistanceToTarget <= _maxDistanceToTarget)
        {
            SetNewPosition();
        }
        else
        {
            ResetPosition();
        }
    }

    private void SetNewPosition()
    {
        float newX;
        float newZ;
        newX = Vector3.SmoothDamp(transform.position, _target.position, ref _velocity, _ParallelMovementSmoothTime).x;
        newZ = transform.position.z + _speed * Time.deltaTime;
        transform.position = new Vector3(newX, transform.position.y, newZ);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == _tags.Player)
        {
            _isTargetReached = true;
            GameEvents.Instance.ShadowTrapActivated();
        }
    }
}
