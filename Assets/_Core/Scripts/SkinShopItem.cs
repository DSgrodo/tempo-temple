using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinShopItem : MonoBehaviour
{

    [SerializeField] private SkinShopItemUI _itemUI;
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;

    private int _index = -1;
    private int _price = -1;

    private void Start()
    {
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    public void Setup(int index, int price)
    {
        _index = index;
        _price = price;

        if (PlayerPrefs.GetInt(_playerPrefsKeys.HasAccessToSkin + _index) == 1)
        {
            _itemUI.RemoveBuyButton();
            _itemUI.TurnOnUseButton();
        }

        if (PlayerPrefs.GetInt(_playerPrefsKeys.CurrentSkinIndex) == _index)
        {
            _itemUI.AddOutline();
        }
        else
        {
            _itemUI.RemoveOutline();
        }

    }

    public void Use()
    {
        GameEvents.Instance.SkinUpdate();
        PlayerPrefs.SetInt(_playerPrefsKeys.CurrentSkinIndex, _index);
        _itemUI.AddOutline();
    }

    public void Buy()
    {
        int coinsAmount = PlayerPrefs.GetInt(_playerPrefsKeys.CoinsAmount);
        if (coinsAmount >= _price)
        {
            UpdateAccess();
            UpdateCoinsAmount(coinsAmount);
            UpdateCoinsUI();
            UpdateButtons();
        }
    }

    private void SubscribeToEvents()
    {
        GameEvents.Instance.OnSkinUpdate += OnSkinUpdateHandler;
    }

    private void UnsubscribeFromEvents()
    {
        GameEvents.Instance.OnSkinUpdate -= OnSkinUpdateHandler;
    }

    private void OnSkinUpdateHandler()
    {
        _itemUI.RemoveOutline();
    }

    private void UpdateCoinsAmount(int coinsAmount)
    {
        PlayerPrefs.SetInt(_playerPrefsKeys.CoinsAmount, coinsAmount - _price);
    }

    private void UpdateAccess()
    {
        PlayerPrefs.SetInt(_playerPrefsKeys.HasAccessToSkin + _index, 1);
    }

    private void UpdateButtons()
    {
        _itemUI.TurnOnUseButton();
        _itemUI.RemoveBuyButton();
    }

    private static void UpdateCoinsUI()
    {
        GameEvents.Instance.CoinsUpdate();
    }
}
