using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;
    [SerializeField] private UIManager _uiManager;
    [SerializeField] private Transform _player;
    [SerializeField] private PlayerInput _playerInputComponent;
    [SerializeField] private GameObject _UIDpad;
    [SerializeField] private GameObject _PauseMenu;
    private int _collectedCoinsAmount = 0;
    private int _score = 0;

    private void Start()
    {
        SubscribeToEvents();
        HandleMobileInputType();
    }

    private void Update()
    {
        if (PlayerExist())
        {
            UpdateScore();
        }
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }


    private void HandleMobileInputType()
    {
        if (PlayerPrefs.GetInt(_playerPrefsKeys.ControlsType) == 0)
        {
            _playerInputComponent.enabled = false;
            _UIDpad.SetActive(true);
        }
    }

    private void UpdateScore()
    {
        int newScore = (int)_player.position.z;
        if (newScore > _score)
        {
            _score = newScore;
            _uiManager.SetScore(_score);
        }
    }

    private bool PlayerExist()
    {
        return _player != null;
    }

    private void SubscribeToEvents()
    {
        GameEvents.Instance.OnPickedUpCoin += OnPickedUpCoinHandler;
        GameEvents.Instance.OnPlayerDestroyed += OnPlayerDestroyedHandler;
    }

    private void UnsubscribeFromEvents()
    {
        GameEvents.Instance.OnPickedUpCoin -= OnPickedUpCoinHandler;
        GameEvents.Instance.OnPlayerDestroyed -= OnPlayerDestroyedHandler;
    }

    private void OnPlayerDestroyedHandler()
    {
#if UNITY_ANDROID || UNITY_IOS
        if (_PauseMenu != null)
        {
            if (_PauseMenu.activeInHierarchy == false)
            {
                Handheld.Vibrate();
            }
        }
#endif
        HandleScore();
        UpdateGeneralCoinsAmount();
    }

    public void UpdateGeneralCoinsAmount()
    {
        PlayerPrefs.SetInt(_playerPrefsKeys.CoinsAmount, PlayerPrefs.GetInt(_playerPrefsKeys.CoinsAmount) + _collectedCoinsAmount);
    }

    public void HandleScore()
    {
        if (_score > PlayerPrefs.GetInt(_playerPrefsKeys.Record))
        {
            PlayerPrefs.SetInt(_playerPrefsKeys.Record, _score);
        }
    }

    private void OnPickedUpCoinHandler()
    {
        _collectedCoinsAmount += 1;
        _uiManager.SetCoins(_collectedCoinsAmount);
    }

}
