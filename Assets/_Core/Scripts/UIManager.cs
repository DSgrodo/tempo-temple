using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    private const int coinsOnStart = 0;
    private const int scoreOnStart = 0;
    [SerializeField] private TMP_Text _score;
    [SerializeField] private TMP_Text _coins;
    private string _defaultScoreText;
    private string _defaultCoinsText;

    private void Start()
    {
        InitTexts();
    }


    private void InitTexts()
    {
        _defaultScoreText = _score.text;
        _defaultCoinsText = _coins.text;
        _score.text = _defaultScoreText + " " + scoreOnStart.ToString();
        _coins.text = _defaultCoinsText + " " + coinsOnStart.ToString();
    }

    public void SetCoins(int value)
    {
        _coins.text = _defaultCoinsText + " " + value.ToString();
    }

    public void SetScore(int value)
    {
        _score.text = _defaultScoreText + " " + value.ToString();
    }
}