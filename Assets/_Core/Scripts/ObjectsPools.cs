using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsPools : MonoBehaviour
{
    [SerializeField] private List<GameObject> _objectsToPool;
    [SerializeField] private readonly int _StartAmountToPool = 1;
    private List<List<GameObject>> _pools = new List<List<GameObject>>();

    private void Awake()
    {
        InitAllPools();
    }

    private void InitAllPools()
    {
        List<GameObject> newPool;
        for (int i = 0; i < _objectsToPool.Count; i++)
        {
            newPool = InitPool(_objectsToPool[i]);
            _pools.Add(newPool);
        }
    }

    private List<GameObject> InitPool(GameObject objectToPool)
    {
        List<GameObject> pool = new List<GameObject>();
        for (int i = 0; i < _StartAmountToPool; i++)
        {
            AddObjectToPool(objectToPool, pool);
        }
        return pool;
    }

    private GameObject AddObjectToPool(GameObject objectToPool, List<GameObject> pool)
    {
        GameObject newObject = Instantiate(objectToPool);
        newObject.SetActive(false);
        pool.Add(newObject);
        return newObject;
    }

    public GameObject GetPooledObject(int poolIndex)
    {
        for (int i = 0; i < _pools[poolIndex].Count; i++)
        {
            if (_pools[poolIndex][i].activeInHierarchy == false)
            {
                return _pools[poolIndex][i];
            }
        }
        GameObject newObject = AddObjectToPool(_objectsToPool[poolIndex], _pools[poolIndex]);
        return newObject;
    }

    public Transform SpawnObjectFromPool(int poolIndex, Vector3 position, Quaternion rotation)
    {
        GameObject objectFromPool = GetPooledObject(poolIndex);
        objectFromPool.transform.SetPositionAndRotation(position, rotation);
        objectFromPool.SetActive(true);
        return objectFromPool.transform;
    }

    public Transform SpawnObjectFromRandomPool(Vector3 position, Quaternion rotation)
    {
        return SpawnObjectFromPool(Random.Range(0, _pools.Count), position, rotation);
    }

}
