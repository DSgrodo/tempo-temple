using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEnablerDisabler : MonoBehaviour
{
    [SerializeField] private float _activeTime = 1f;
    [SerializeField] private float _inactiveTime = 1f;
    [SerializeField] private GameObject _targetObject;
    [SerializeField] private bool _enabledOnStart = true;
    [SerializeField] private float _countedTime = 0f;

    private void Start()
    {
        SetState();
    }

    private void Update()
    {
        HandleOnOffLoop();
    }


    private void SetState()
    {
        _targetObject.SetActive(_enabledOnStart);
    }

    private void HandleOnOffLoop()
    {
        if (_targetObject.activeInHierarchy == true)
        {
            if (_countedTime >= _activeTime)
            {
                _targetObject.SetActive(false);
                _countedTime = 0f;
            }
            _countedTime += Time.deltaTime;
        }
        else
        {
            if (_countedTime >= _inactiveTime)
            {
                _targetObject.SetActive(true);
                _countedTime = 0f;
            }
            _countedTime += Time.deltaTime;
        }
    }
}
