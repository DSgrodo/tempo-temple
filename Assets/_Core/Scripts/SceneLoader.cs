using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private GameObject _loadingScreen;
    [SerializeField] private Slider _progressBar;
    public static SceneLoader Instance { get; private set; }

    private void Awake()
    {
        InitSingleton();
    }


    private void InitSingleton()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadAsynchronously(sceneName));
    }

    private IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        _loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            FillProgressBar(operation);
            yield return null;
        }
    }

    private IEnumerator LoadAsynchronously(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        _loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            FillProgressBar(operation);
            yield return null;
        }
    }

    private void FillProgressBar(AsyncOperation operation)
    {
        float progress = Mathf.Clamp01(operation.progress / 0.9f);
        _progressBar.value = progress;
    }
}
