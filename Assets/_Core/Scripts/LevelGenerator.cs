using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    private const float spawnDistance = 20f;
    private const string endPoint = "EndPoint";
    [SerializeField] private readonly int allowedPartsNumber = 5;
    [SerializeField] private Transform _player;
    [SerializeField] private Transform _startPart;
    [SerializeField] private ObjectsPools _levelPartsPools;
    private Vector3 _lastPartEndPoint;
    private Queue<GameObject> _existingParts = new Queue<GameObject>();


    private void Start()
    {
        InitStartPart();
        SpawnNewPart();
    }

    private void Update()
    {
        if (_player != null)
        {
            if (PlayerCloseToCurrentPartsEnd())
            {
                SpawnNewPart();
                RemoveOldPart();
            }
        }

    }

    private void InitStartPart()
    {
        FindNewEndPoint(_startPart);
        _existingParts.Enqueue(_startPart.gameObject);
    }

    private void FindNewEndPoint(Transform part)
    {
        _lastPartEndPoint = part.transform.Find(endPoint).position;
    }

    private void SpawnNewPart()
    {
        Transform lastPart = _levelPartsPools.SpawnObjectFromRandomPool(_lastPartEndPoint, Quaternion.identity);
        FindNewEndPoint(lastPart);
        _existingParts.Enqueue(lastPart.gameObject);
    }

    private void RemoveOldPart()
    {
        if (_existingParts.Count > allowedPartsNumber)
        {
            _existingParts.Dequeue().SetActive(false);
            if (_startPart != null)
            {
                Destroy(_startPart.gameObject);
            }
        }
    }

    private bool PlayerCloseToCurrentPartsEnd()
    {
        return Vector3.Distance(_player.position, _lastPartEndPoint) < spawnDistance;
    }

}
