using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SkinsContainer : ScriptableObject
{
    public List<SkinInfo> list;
}
