using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TagsContainer : ScriptableObject
{
    public readonly string Environment = "Environment";
    public readonly string PickUp = "PickUp";
    public readonly string Player = "Player";
}
