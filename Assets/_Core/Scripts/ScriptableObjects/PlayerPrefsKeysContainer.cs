using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerPrefsKeysContainer : ScriptableObject
{
    //PC options
    public readonly string IsFullscreen = "isFullscreen";
    public readonly string ResolutionIndex = "resolutionIndex";

    //Mobile options
    public readonly string ControlsType = "controlsType";

    //Common options
    public readonly string GraphicsLevelIndex = "graphicsLevelIndex";
    public readonly string GeneralVolume = "generalVolume";
    public readonly string MusicVolume = "musicVolume";
    public readonly string EffectsVolume = "effectsVolume";

    public readonly string Record = "record";
    public readonly string CoinsAmount = "coinsAmount";

    public readonly string CurrentSkinIndex = "currentSkinIndex";
    public readonly string SkinsAmount = "skinsAmount";
    public readonly string HasAccessToSkin = "hasAccessToSkin"; // + skin number (e.g. hasAccessToSkin1, hasAccessToSkin2, ...)

}
