using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragileTileTrap : MonoBehaviour
{
    private const string animationTrigger = "SteppedOn";
    [SerializeField] private TagsContainer _tags;
    [SerializeField] private GameObject _tile;
    [SerializeField] private float _delayTime = 2f;
    [SerializeField] private Animator _animator;
    [SerializeField] private ParticleSystem _tileBrokenParticleSystem;
    [SerializeField] private AudioSource _shakingSound;
    [SerializeField] private AudioSource _breakSound;
    private bool _isTriggered = false;


    private void OnEnable()
    {
        ResetTrap();
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == _tags.Player && _isTriggered == false)
        {
            _isTriggered = true;
            _shakingSound.Play();
            _animator.SetTrigger(animationTrigger);
            Invoke(nameof(RemoveTile), _delayTime);
        }
    }


    private void RemoveTile()
    {
        _tileBrokenParticleSystem.Play();
        _tile.SetActive(false);
        _shakingSound.Stop();
        _breakSound.Play();
    }

    private void ResetTrap()
    {
        _isTriggered = false;
        _tile.SetActive(true);
    }
}
