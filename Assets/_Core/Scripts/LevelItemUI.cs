using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelItemUI : MonoBehaviour
{
    [SerializeField] private TMP_Text _index;

    public void Setup(int index)
    {
        _index.text = index.ToString();
    }
}
