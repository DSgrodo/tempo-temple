using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinLoader : MonoBehaviour
{
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;
    [SerializeField] private SkinsContainer _skins;

    void Start()
    {
        LoadSkin();
    }

    private void LoadSkin()
    {
        GameObject skinPrefab = _skins.list[PlayerPrefs.GetInt(_playerPrefsKeys.CurrentSkinIndex)].model;
        Instantiate(skinPrefab, transform);
    }
}
