using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SkinInfo
{
    public GameObject model;
    public Sprite sprite;
    public string name = "NAME";
    public int price = 100;
    public bool availableOnStart = false;
}
